from setuptools import find_packages, setup


setup(
    name='weather-forecast-chart',
    version='0.4.0',

    author='Stefan Bunde',
    author_email='stefanbunde+git@posteo.de',

    python_requires='~=3.6',
    install_requires=[
        'selenium',
    ],
    extras_require={
        'testing': ['tox'],
    },

    packages=find_packages(),

    entry_points={
        'console_scripts': [
            'weather-forecast-chart=weather_forecast_chart.weather_forecast_chart:main',
        ],
    },

    classifiers=[
        'Development Status :: 4 - Beta',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3 :: Only',
        'Programming Language :: Python :: 3.6',
    ],
)
