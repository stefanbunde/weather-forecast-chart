import argparse
import os

from selenium import webdriver


def get_chart(driver, days):
    driver.get(f'https://www.wetter.com/wetter_aktuell/wettervorhersage/{days}_tagesvorhersage/'
               'deutschland/leipzig/DE0006194.html')
    element = driver.find_element_by_id(f'chartdiv-{days}')
    return element.get_property('outerHTML')


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--chrome', action='store_true')
    parser.add_argument('-d', '--days', default=16, choices=['3', '7', '16'])
    parser.add_argument('--firefox', action='store_true')
    parser.add_argument(
        '-o', '--output-file',
        default=os.path.join(os.getcwd(), 'weather-forecast-chart.html')
    )
    args = parser.parse_args()

    if args.chrome:
        driver = webdriver.Chrome()
    elif args.firefox:
        driver = webdriver.Firefox(service_log_path=os.path.devnull)
    else:
        driver = webdriver.Chrome()

    try:
        chart = get_chart(driver, args.days)
    except Exception as e:
        print(f'failed - {e}')
    finally:
        driver.close()

    with open(args.output_file, 'w') as fh:
        fh.write(chart)


if __name__ == '__main__':
    main()
