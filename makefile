.PHONY: build


build:
	python setup.py sdist bdist_wheel

clean:
	rm -rf build/ dist/ weather_forecast_chart.egg-info/
